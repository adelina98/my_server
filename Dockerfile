FROM debian:buster

#install nginx
RUN apt-get -y update 
#wget - a computer program that retrieves content from web servers
RUN apt-get -y install wget
RUN apt-get -y install nginx

#install MySQL (MariaDB)
#система управления базами данных MySQL на debian
RUN apt-get -y install mariadb-server

#install php
#php-fpm -> Fastcgi Process Manager - программный пакет, позволяющий выполнить обработку скриптов, написанных на языке PHP.
#Nginx requires an external program to handle PHP processing and act as bridge between the PHP interpreter itself and the web server.
RUN apt-get -y install php-fpm php-mysql

#communicate php with wordpress and sql
#some of the most popular PHP extensions for use with WordPress
RUN apt-get -y install php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip

#download and extract wordpress application or use ADD command
RUN wget http://wordpress.org/latest.tar.gz
RUN tar -xzvf latest.tar.gz 

#download and extract phpmyadmin application
RUN wget https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-english.tar.gz
RUN tar -xzf phpMyAdmin-4.9.0.1-english.tar.gz

#create a self-signed key and certificate pair with OpenSSL in a single command:
#-new - создает новый запрос;
#-newkey rsa:2048 - генерирует новый ключ размером 2048 бит;
#-nodes - не шифрует ключ;
RUN openssl req \
    -x509 -nodes -days 365 -newkey rsa:2048 
    #указывает файл, в котором хранить ключ
    -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt 
    #subj command to disable question prompts when generating a CSR 
    #If you want to non-interactively answer the CSR information prompt, you can do so by adding the -subj option to any OpenSSL commands that request CSR information.
    #localhost - имя вашего домена
    -subj "/C=ru/ST=Moscow/L=Moscow/O=no/OU=no/CN=localhost"

COPY srcs/config.inc.php phpMyAdmin-4.9.0.1-english/
COPY srcs/wp-config.php wordpress/
COPY srcs/init.sql .
COPY srcs/nginx_config /etc/nginx/sites-available/
COPY srcs/autoindex.sh .
COPY srcs/start.sh .

#/var/www/html is just the default root folder of the web server. 
#You can change that to be whatever folder you want by editing your apache.conf file (usually located in /etc/apache/conf) and changing the DocumentRoot
#Activate your configuration by linking to the config file from Nginx’s sites-enabled directory
#This will tell Nginx to use the configuration next time it is reloaded
RUN ln -s /etc/nginx/sites-available/nginx_config /etc/nginx/sites-enabled/
RUN rm /etc/nginx/sites-enabled/default

#the root web directory
RUN mkdir /var/www/localhost

#assign ownership of the directory with the $USER environment variable, which should reference your current system user
#Now that our files are in place, we’ll assign ownership them to the www-data user and group. This is the user and group that Nginx runs as, and Nginx will need to be able to read and write WordPress files in order to serve the website and perform automatic updates
RUN chown -R www-data:www-data /var/www/*
RUN chmod -R 755 /var/www/*

RUN mv phpMyAdmin-4.9.0.1-english /var/www/localhost/phpmyadmin
RUN mv wordpress/ /var/www/localhost/wordpress

RUN service mysql start && mysql < init.sql 

EXPOSE 80 443

#описывает команду с аргументами, которую нужно выполнить когда контейнер будет запущен
ENTRYPOINT ["bash","start.sh"]


#you’ve built a flexible foundation for serving PHP websites and applications to your visitors, using Nginx as web server. You’ve set up Nginx to handle PHP requests through php-fpm, and you also set up a MariaDB database to store your website’s data.