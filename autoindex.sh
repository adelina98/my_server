#!/bin/bash
if grep "autoindex on" /etc/nginx/sites-available/nginx_config ##Конфигурация вашего веб-сайта будет храниться в папке
then
    sed -i 's/autoindex on/autoindex off/' /etc/nginx/sites-available/nginx_config
    echo "autoindex disabled"
    service nginx restart
elif grep -q "autoindex off" /etc/nginx/sites-available/nginx_config
then
    sed -i 's/autoindex off/autoindex on/' /etc/nginx/sites-available/nginx_config
    echo "autoindex enabled"
    service nginx restart
fi
